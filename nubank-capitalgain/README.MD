# Nubank - Capital Gain - Calculate Taxes

## Execução
A aplicação pode ser executada das seguintes formas: 

  - Docker (Executar a aplicação utilizando Docker): 
    * Executar o comando: `docker-compose run app`
    * Executar o script `./startup.sh`
    * Entrar com a linha do json, ex: `[{"operation":"buy", "unit-cost":10.00, "quantity": 10000},` e pressionar Enter
    * Para sair da aplicação digitar `q` e pressionar Enter;
    * Ou informar um arquivo na chamada da aplicação, ex: `./startup.sh < input.txt` (este input.txt precisa estar no diretório da aplicação, tem um de exemplo aqui)
    
  - Local (precisa de Java 17 e maven instalado na maquina): 
    * Entrar no diretório da aplicação
    * Compilar usando `mvn package`
    * Executar o script `./startup.sh`
    * Entrar com a linha do json, ex: `[{"operation":"buy", "unit-cost":10.00, "quantity": 10000},` e pressionar Enter
    * Para sair da aplicação digitar `q` e pressionar Enter;
    * Ou informar um arquivo na chamada da aplicação, ex: `./startup.sh < input.txt` (este input.txt precisa estar no diretório da aplicação, tem um de exemplo aqui)
    
* Obs: O script `./startup.sh` é apenas um utilitário, pode executar a aplicação também usando: `java -jar target/app.jar`

## Arquitetura 
O projeto foi realizado em Java 17 com Spring Boot e maven, principalmente pelo fato de ser minha linguagem de domínio, onde posso expressar melhor minha experiência com programação. 
Também considerei ser um exercício para simular o ambiente produtivo de trabalho, e o Java é uma ótima linguagem para aplicações enterprise de grande porte (apesar deste ser um exercício simples, ainda assim, o desenvolvimento com Spring Boot torna o projeto produtivo e simples de ser executado, seja web ou command line).
Fácil também de ser deployado como micro serviço;

O projeto foi dividido em camadas cada uma com sua responsabilidade.
  
  - Service para execução da lógica de negócio;
  - Repository para armazenamento dos dados. Hoje está sendo salvo em uma lista local, mas ficou bastante fácil de mudar a fonte de dados;
  - Entity e DTOs para representar o modelo;
  - Teríamos o controller caso fossemos usar Rest, mas ficamos apenas com a classe principal (Main) chamando diretamente a Service, passando os argumentos recebidos;
  - Classes auxiliares como Json converter, etc;
  - Nenhuma classe guarda estado. A única que está guardando estado é o Repository, porque representa o banco de dados;
  - Nenhuma classe ou método faz além daquilo para o qual foi criada, escopo e responsabilidades bem definidas.
  - Métodos de baixa complexidade e fácil entendimento;
  - Como foi pedido para não colocar banco de dados, não foi realizado controle de transação;
  - Variáveis e parâmetros, sempre que possível declarado como final;
  - Foi utilizado Lombok para evitar excesso de boilerplate;
  - Uso de BigDecimal para evitar erros de cálculos;
  - No objeto de resultado `CalculateTaxResponseDTO` foi utilizado um objeto do tipo record do Java, que é um objeto imutável.

A estratégia adotada para realizar o cálculo do imposto foi a seguinte: 
  
  - Para cada linha processada do Json é efetuado o cálculo do preço médio e somando a quantidade total da compra;
  - Para o preço médio da próxima compra, é considerado o preço médio anterior;
  - Para vendas, o registro salvo carrega o preço médio e quantidade do último registro de compra, além dos prejuízos a deduzir na próxima venda, caso exista.
  - Para realizar o cálculo do preço médio, é necessário apenas o último registro de preço médio salvo. Não é necessário percorrer todos os registros novamente, para efetuar a soma. Melhor performance, mesmo que tenha milhares de registros.

A aplicação ficou de fácil manutenção, fácil para continuar crescendo e adicionando funcionalidades. Hoje está usando linha de comando, mas pode facilmente ser convertido para operações Rest e também acrescentado uma fonte de dados (Banco de dados qualquer), com mínimo de impacto na aplicação.

Como disse, utilizei Java por ser minha linguagem principal de trabalho do dia-a-dia, mas também poderia ser feito em outras linguagens que também tenho domínio, como Dart da google (do framework Flutter), JavaScript (React, Nodejs).

# Tests
Foi utilizado Junit para criar os testes. 
Ao compilar o projeto, os testes são executados, ou pode-se digitar o comando `mvn test`.

O teste abrange toda lógica de cálculo do imposto. Dado um json de entrada, após todo o processamento espera-se um determinado resultado para validar o teste.