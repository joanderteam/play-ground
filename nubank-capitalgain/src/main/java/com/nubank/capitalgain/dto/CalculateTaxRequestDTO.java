package com.nubank.capitalgain.dto;

import com.google.gson.annotations.SerializedName;
import com.nubank.capitalgain.enums.OperationTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class CalculateTaxRequestDTO {
    private OperationTypeEnum operation;
    @SerializedName("unit-cost")
    private BigDecimal unitCost;
    private Long quantity;
}
