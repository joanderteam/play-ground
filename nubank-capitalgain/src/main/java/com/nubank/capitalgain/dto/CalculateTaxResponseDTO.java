package com.nubank.capitalgain.dto;

import java.math.BigDecimal;

public record CalculateTaxResponseDTO(BigDecimal tax) {
}
