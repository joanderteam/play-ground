package com.nubank.capitalgain.service;

import com.nubank.capitalgain.entity.Operation;
import com.nubank.capitalgain.repository.OperationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class OperationService {

    private final OperationRepository operationRepository;

    public Optional<Operation> findLastAveragePrice() {
        return operationRepository.findLast();
    }

    public List<Operation> saveAll(List<Operation> operations) {
        return operationRepository.saveAll(operations);
    }
}
