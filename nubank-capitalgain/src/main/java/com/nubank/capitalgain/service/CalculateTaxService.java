package com.nubank.capitalgain.service;

import com.nubank.capitalgain.dto.CalculateTaxRequestDTO;
import com.nubank.capitalgain.dto.CalculateTaxResponseDTO;
import com.nubank.capitalgain.entity.Operation;
import com.nubank.capitalgain.enums.OperationTypeEnum;
import com.nubank.capitalgain.enums.TaxEnum;
import com.nubank.capitalgain.json.JsonConverter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.nubank.capitalgain.enums.OperationTypeEnum.BUY;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_EVEN;
import static org.springframework.util.CollectionUtils.isEmpty;

@Slf4j
@AllArgsConstructor
@Service
public class CalculateTaxService {

    private final OperationService operationService;
    private final JsonConverter jsonConverter;

    public List<CalculateTaxResponseDTO> calculate(final String json) {
        return calculate(jsonConverter.toList(json, CalculateTaxRequestDTO.class));
    }

    public List<CalculateTaxResponseDTO> calculate(final List<CalculateTaxRequestDTO> requestDTOs) {
        if (isEmpty(requestDTOs)) {
            return new ArrayList<>();
        }

        Operation lastOperation = operationService.findLastAveragePrice().orElse(null);
        List<Operation> currentOperations = new ArrayList<>();
        List<CalculateTaxResponseDTO> taxes = new ArrayList<>();

        for (CalculateTaxRequestDTO dto : requestDTOs) {
            if (lastOperation != null) {
                if (BUY.equals(dto.getOperation())) {
                    lastOperation = calculateAveragePriceLongPosition(dto, lastOperation);
                } else {
                    lastOperation = calculateTax(dto, lastOperation);
                }
            } else {
                lastOperation = openLongPosition(dto);
            }
            currentOperations.add(lastOperation);
            taxes.add(new CalculateTaxResponseDTO(lastOperation.getTax()));
        }

        operationService.saveAll(currentOperations);

        return taxes;
    }

    private Operation calculateAveragePriceLongPosition(final CalculateTaxRequestDTO dto, final Operation lastOperation) {
        var totalQuantity = lastOperation.getQuantity() + dto.getQuantity();
        var previousAmount = lastOperation.getAveragePrice().multiply(new BigDecimal(lastOperation.getQuantity()));
        var currentAmount = dto.getUnitCost().multiply(new BigDecimal(dto.getQuantity()));
        var totalAmount = previousAmount.add(currentAmount);
        var averagePrice = totalAmount.divide(new BigDecimal(totalQuantity), 2, HALF_EVEN);

        return Operation.builder()
                .operationTypeEnum(BUY)
                .averagePrice(averagePrice)
                .deductibleLoss(lastOperation.getDeductibleLoss())
                .tax(ZERO)
                .quantity(totalQuantity)
                .date(OffsetDateTime.now())
                .build();
    }

    private Operation calculateTax(final CalculateTaxRequestDTO dto, final Operation lastOperation) {
        var currentQuantity = new BigDecimal(dto.getQuantity());
        var currentValue = dto.getUnitCost().multiply(currentQuantity);
        var averagePriceValue = currentQuantity.multiply(lastOperation.getAveragePrice());
        var result = currentValue.subtract(averagePriceValue);
        var consolidatedResult = result.add(lastOperation.getDeductibleLoss());
        var deductibleLoss = ZERO;
        var tax = ZERO;
        if (consolidatedResult.compareTo(ZERO) <= 0) {
            deductibleLoss = consolidatedResult;
        } else if (currentValue.compareTo(TaxEnum.IR_DAY_TRADE.getMinAmountForTax()) > 0) {
            tax = consolidatedResult.multiply(TaxEnum.IR_DAY_TRADE.getTax()).setScale(2, HALF_EVEN);
        }

        return Operation.builder()
                .operationTypeEnum(OperationTypeEnum.SELL)
                .date(OffsetDateTime.now())
                .quantity(lastOperation.getQuantity())
                .averagePrice(lastOperation.getAveragePrice())
                .deductibleLoss(deductibleLoss)
                .tax(tax)
                .build();
    }

    private Operation openLongPosition(final CalculateTaxRequestDTO dto) {
        return Operation.builder()
                .operationTypeEnum(BUY)
                .deductibleLoss(ZERO)
                .tax(ZERO)
                .date(OffsetDateTime.now())
                .quantity(dto.getQuantity())
                .averagePrice(dto.getUnitCost())
                .build();
    }


}
