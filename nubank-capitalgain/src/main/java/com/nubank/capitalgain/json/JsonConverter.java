package com.nubank.capitalgain.json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.StringUtils.hasLength;

@Component
public class JsonConverter {

    private final Gson gson = new Gson();

    public <T> String toJson(T object) {
        return gson.toJson(object);
    }

    public <T> List<T> toList(final String rawJson, Class<T> type) {
        try {
            if (!hasLength(rawJson)) {
                return new ArrayList<>();
            }
            var json = rawJson.replaceAll("\\s+", "");
            if (!json.startsWith("[")) {
                json = "[" + json;
            }
            if (!json.endsWith("]")) {
                json += "]";
            }
            if (json.charAt(json.length() - 2) == ',') {
                json = json.replace(",]", "]");
            }

            return gson.fromJson(json, TypeToken.getParameterized(List.class, type).getType());
        } catch (Exception e) {
            throw new IllegalStateException("Invalid json");
        }
    }

}
