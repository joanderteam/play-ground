package com.nubank.capitalgain.entity;

import com.nubank.capitalgain.enums.OperationTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Operation {
    private Long id;
    private OffsetDateTime date;
    private Long quantity;
    private OperationTypeEnum operationTypeEnum;
    private BigDecimal averagePrice;
    private BigDecimal deductibleLoss;
    private BigDecimal tax;
}
