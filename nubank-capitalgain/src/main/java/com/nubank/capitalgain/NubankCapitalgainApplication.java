package com.nubank.capitalgain;

import com.nubank.capitalgain.json.JsonConverter;
import com.nubank.capitalgain.service.CalculateTaxService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@SpringBootApplication
public class NubankCapitalgainApplication implements CommandLineRunner {

    private final CalculateTaxService calculateTaxService;
    private final JsonConverter jsonConverter;

    public static void main(String[] args) {
        SpringApplication.run(NubankCapitalgainApplication.class, args);
    }

    @Override
    public void run(String... args) throws IOException {
        log.info("Ready!\n\n\n\n");
        final var reader = new BufferedReader(new InputStreamReader(System.in));
        if (reader.ready()) {
            processEntireStream(reader);
        } else {
            processLine(reader);
        }
        log.info("\n\n\n\nFinished");
    }

    private void processLine(BufferedReader reader) {
        reader.lines().forEach(line -> {
            if ("q".equals(line)) {
                exit(reader);
            }
            try {
                log.info(jsonConverter.toJson(calculateTaxService.calculate(line).get(0)));
            } catch (Exception e) {
                log.warn(e.getMessage());
            }
        });
    }

    private void processEntireStream(BufferedReader reader) {
        Optional<String> json = reader.lines().reduce((s, s2) -> s + s2);
        try {
            if (json.isPresent()) {
                log.info("Result:");
                log.info(jsonConverter.toJson(calculateTaxService.calculate(json.get())));
            } else {
                log.warn("Empty json");
            }
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }

    private void exit(BufferedReader reader) {
        try {
            reader.close();
            System.exit(0);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
