package com.nubank.capitalgain.enums;

import java.math.BigDecimal;

public enum TaxEnum {
    IR_DAY_TRADE(new BigDecimal("20000"), new BigDecimal("0.2"));

    TaxEnum(final BigDecimal minAmountForTax, final BigDecimal tax) {
        this.minAmountForTax = minAmountForTax;
        this.tax = tax;
    }

    private final BigDecimal minAmountForTax;
    private final BigDecimal tax;

    public BigDecimal getMinAmountForTax() {
        return minAmountForTax;
    }

    public BigDecimal getTax() {
        return tax;
    }
}
