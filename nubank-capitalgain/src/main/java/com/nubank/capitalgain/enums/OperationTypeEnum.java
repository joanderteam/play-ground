package com.nubank.capitalgain.enums;

import com.google.gson.annotations.SerializedName;

public enum OperationTypeEnum {
    @SerializedName("buy")
    BUY("buy"),
    @SerializedName("sell")
    SELL("sell");

    OperationTypeEnum(String type) {
        this.type = type;
    }

    private final String type;

    public String getType() {
        return type;
    }


}
