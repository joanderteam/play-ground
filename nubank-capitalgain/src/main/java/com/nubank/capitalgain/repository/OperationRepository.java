package com.nubank.capitalgain.repository;

import com.nubank.capitalgain.entity.Operation;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class OperationRepository {

    private final List<Operation> database = new ArrayList<>();
    private final AtomicLong ids = new AtomicLong(0);

    public List<Operation> saveAll(final List<Operation> operations) {
        final var newOperations = operations.stream().map(op -> op.toBuilder()
                .id(ids.incrementAndGet()).build()).toList();
        database.addAll(newOperations);
        return newOperations;
    }

    public Optional<Operation> findLast() {
        if (database.size() == 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(database.get(database.size() - 1));
    }


}
