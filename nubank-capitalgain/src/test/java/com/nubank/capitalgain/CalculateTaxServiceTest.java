package com.nubank.capitalgain;

import com.nubank.capitalgain.json.JsonConverter;
import com.nubank.capitalgain.repository.OperationRepository;
import com.nubank.capitalgain.service.CalculateTaxService;
import com.nubank.capitalgain.service.OperationService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;

public class CalculateTaxServiceTest {

    private CalculateTaxService calculateTaxService;
    private JsonConverter jsonConverter;

    @BeforeEach
    private void setup() {
        OperationRepository operationRepository = new OperationRepository();
        OperationService operationService = new OperationService(operationRepository);
        jsonConverter = new JsonConverter();
        calculateTaxService = new CalculateTaxService(operationService, jsonConverter);
    }

    @Test
    public void giveInput_calculateTaxes_expectResult_thenCorret() throws IOException {
        String result = jsonConverter.toJson(calculateTaxService.calculate(getInputTest()));
        Assertions.assertEquals(result, getResultTest());
    }

    private String getInputTest() throws IOException {
        FileInputStream fis = new FileInputStream("src/test/resources/input-test.json");
        return IOUtils.toString(fis, UTF_8);
    }

    private String getResultTest() throws IOException {
        FileInputStream fis = new FileInputStream("src/test/resources/result-test.json");
        return IOUtils.toString(fis, UTF_8);
    }

}
